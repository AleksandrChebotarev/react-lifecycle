import React from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      stringData: "Строковый тип данных",
      numberData: 25,
      booleanData: true,
      nullData: null,
      list: ["JavaScript", "TypeScript", "C#", "HTML", "CSS", "SASS", "LESS"],
      timerID: null,
    };
  }

  componentDidMount() {
    console.log(
      "componentDidMount() вызывается сразу после монтирования (то есть, вставки компонента в DOM). В этом методе должны происходить действия, которые требуют наличия DOM-узлов."
    );
    this.setState({ timerID: setInterval(() => this.tick(), 1000) });
  }

  componentDidUpdate() {
    console.log(
      "componentDidUpdate() вызывается сразу после обновления state. Не вызывается при первом рендере. Метод позволяет работать с DOM при обновлении компонента"
    );
  }

  componentWillUnmount() {
    console.log(
      "componentWillUnmount() вызывается непосредственно перед размонтированием и удалением компонента. В этом методе выполняется необходимый сброс: отмена таймеров, сетевых запросов и подписок, созданных в componentDidMount()"
    );
    this.setState({ timerID: clearInterval(this.state.timerID) });
  }

  tick() {
    this.setState({ date: new Date() });
  }

  render() {
    const { date, stringData, numberData, booleanData, nullData, list } =
      this.state;

    return (
      <div className="App">
        <header className="App-header">
          <time>{date.toString()}</time>
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Измените функциональный компонент <code>src/App.js</code> на
            классовый и сохраните!
          </p>
          <div className="App-ul-container">
            <h3 className="App-ul-title">Список типов данных</h3>
            <ul className="App-ul">
              <li>{stringData}</li>
              <li>{numberData}</li>
              <li>{`Output: ${String(
                booleanData
              )}, but Booleans, Null, and Undefined are ignored in JSX React`}</li>
              <li>{`Output: ${String(
                nullData
              )}, but Booleans, Null, and Undefined are ignored in JSX React`}</li>
            </ul>
          </div>
          <div className="App-ul-container">
            <h3 className="App-ul-title">Список языков из массива</h3>
            <ul className="App-ul">
              {list.map((item, index) => (
                <li key={index}>{item}</li>
              ))}
            </ul>
          </div>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Изучайть React на официальном сайте!
          </a>
        </header>
      </div>
    );
  }
}

export default App;
